import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class App {

    public static BigInteger nextRandomBigInteger(BigInteger n) {
        Random rand = new Random();
        BigInteger result = new BigInteger(n.bitLength(), rand);
        while( result.compareTo(n) >= 0 ) {
            result = new BigInteger(n.bitLength(), rand);
        }
        return result;
    }

    public static BigInteger findE(BigInteger f){
        BigInteger rand = nextRandomBigInteger(f);
        while(true){
            if(rand.gcd(f).equals(BigInteger.valueOf(1)))
                return rand;
            else
                rand = nextRandomBigInteger(f);
        }
    }

    public static void main(String[] args) {

        String message = "Je suis le message a chiffrer et a hasher.";
        System.out.println("Original message to send is: " + message + "\n");

        Random rand = new Random();
        BigInteger p = new BigInteger(512, 100, rand);
        BigInteger q = new BigInteger(512, 100, rand);
        BigInteger phi = p.subtract(BigInteger.valueOf(1)).multiply(q.subtract(BigInteger.valueOf(1)));

        BigInteger n = p.multiply(q); // clé publique
        BigInteger e = findE(phi); // ------------
        BigInteger d = e.modInverse(phi); // clé privée


        EncryptAndSign cypher = new EncryptAndSign(message);
        System.out.println("Crypting and hashing message... \n");

        DecryptAndVerify verifier = new DecryptAndVerify();

        ArrayList<String> encryptedMessage = cypher.encryptAndSign(e, n); //TODO: Théo tu dois implémenter cet partie (cryptage
        String hashedMessage = cypher.getHash(); //TODO: et hashage (avec la classe SHA512))

        System.out.println("Sending crypted message...");
        System.out.println("Verifying authenticity...\n");
        verifier.decryptAndVerify(encryptedMessage, hashedMessage, d, n); //TODO: Moi je m'occupe de celle la

    }
}

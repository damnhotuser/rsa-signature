import java.math.BigInteger;
import java.util.ArrayList;

class DecryptAndVerify {

    public void decryptAndVerify(ArrayList<String> encryptedMessage,
                                 String hashedMessage,
                                 BigInteger d, BigInteger n) {
        String orig = "";

        for (String s : encryptedMessage) {
            BigInteger b = new BigInteger(s);
            b = b.modPow(d, n);
            orig += (char) b.intValue();
        }

        System.out.println("Retrieved message: " + orig);

        if (SHA512.hashText(orig).equals(hashedMessage))
            System.out.println("Hash are corresponding! It works!");
    }
}

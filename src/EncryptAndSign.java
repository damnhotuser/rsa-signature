
import java.math.BigInteger;
import java.util.ArrayList;

class EncryptAndSign {
    private String originalMessage = null;

    public EncryptAndSign(String message){
        this.originalMessage = message;
    }

    public ArrayList<String> encryptAndSign(BigInteger e, BigInteger n) {
        ArrayList<String> cypher = new ArrayList<>();

        for (char c : originalMessage.toCharArray()){
            String s = "" + c;
            BigInteger b = new BigInteger(s.getBytes());
            b = b.modPow(e, n);
            cypher.add(b.toString());
        }
        return cypher;
    }

    public String getHash() {
        return SHA512.hashText(originalMessage);
    }
}
